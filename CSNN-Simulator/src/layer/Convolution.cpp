#include "layer/Convolution.h"
using namespace layer;

static RegisterClassParameter<Convolution, LayerFactory> _register("Convolution");

Convolution::Convolution() : Layer3D(_register),
	_annealing(1.0), _min_th(0),_t_obj(0), _lr_th(0), _alpha(0.05f), _beta(0.1f), _prune_threshold(0), _prune_max_threshold(0.28f), _total_cnx(0), _remaining_cnx(0), _croped(0), _cropedVector(), _doPrune(0), _doReinforcement(0), _w(), _th(), _stdp(nullptr), _input_depth(0), _impl(*this) {

	add_parameter("annealing", _annealing, 1.0f);
    add_parameter("min_th", _min_th);
    add_parameter("t_obj", _t_obj);
    add_parameter("lr_th", _lr_th);

    add_parameter("w", _w);
    add_parameter("th", _th);

	add_parameter("alpha", _alpha, 0.05f);
	add_parameter("beta", _beta, 0.1f);
    add_parameter("stdp", _stdp);
	add_parameter("doPrune", _doPrune, 0.0f);
	add_parameter("doReinforcement", _doReinforcement, 0.0f);
	add_parameter("prune_threshold", _prune_threshold, 0.0f);
	add_parameter("prune_max_threshold", _prune_max_threshold, 0.28f);

	add_parameter("totalCnx", _total_cnx, 0.0f);
	add_parameter("remainingCnx", _remaining_cnx, 0.0f);
}


Convolution::Convolution(size_t filter_width, size_t filter_height, size_t filter_number, size_t stride_x, size_t stride_y, size_t padding_x, size_t padding_y) :
	Layer3D(_register, filter_width, filter_height, filter_number, stride_x, stride_y, padding_x, padding_y),
	_annealing(1.0), _min_th(0),_t_obj(0), _lr_th(0), _alpha(0.05f), _beta(0.1f), _prune_threshold(0), _prune_max_threshold(0.3f), _total_cnx(0), _remaining_cnx(0), _croped(0), _cropedVector(), _doPrune(0), _doReinforcement(0), _w(), _th(), _stdp(nullptr), _input_depth(0), _impl(*this){
	add_parameter("annealing", _annealing, 1.0f);
	add_parameter("min_th", _min_th);
    add_parameter("t_obj", _t_obj);
    add_parameter("lr_th", _lr_th);

    add_parameter("w", _w);
    add_parameter("th", _th);

	add_parameter("alpha", _alpha, 0.05f);
	add_parameter("beta", _beta, 0.1f);
    add_parameter("stdp", _stdp);
	add_parameter("doPrune", _doPrune, 0.0f);
	add_parameter("doReinforcement", _doReinforcement, 0.0f);
	add_parameter("prune_threshold", _prune_threshold, 0.0f);
	add_parameter("prune_max_threshold", _prune_max_threshold, 0.28f);
	add_parameter("totalCnx", _total_cnx, 0.0f);
	add_parameter("remainingCnx", _remaining_cnx, 0.0f);
}

void Convolution::resize(const Shape& previous_shape, std::default_random_engine& random_generator) {
	Layer3D::resize(previous_shape, random_generator);

	_input_depth = previous_shape.dim(2);

	parameter<Tensor<float>>("w").shape(_filter_width, _filter_height, _input_depth, _filter_number);
	parameter<Tensor<float>>("th").shape(_filter_number);
	parameter<float>("totalCnx").set(_filter_width*_filter_height*_input_depth*_filter_number);
	parameter<float>("remainingCnx").set(_filter_width*_filter_height*_input_depth*_filter_number);
	_impl.resize();
	_cropedVector = Tensor<float>(Shape({_filter_width, _filter_height, _filter_number, _input_depth}));
	_cropedVector.fill(0.0f);
}


void Convolution::train(const std::string&, const std::vector<Spike>& input_spike, const Tensor<Time>& input_time, std::vector<Spike>& output_spike) {
	_impl.train(input_spike, input_time, output_spike);
}


void Convolution::test(const std::string&, const std::vector<Spike>& input_spike, const Tensor<Time>& input_time, std::vector<Spike>& output_spike) {
	_impl.test(input_spike, input_time, output_spike);
}

void Convolution::on_epoch_end() {
	_lr_th *= _annealing;
	auto minW = 1.0f;
	auto maxW = 0.0f;

	//std::cout << "Total cnx before : " << _total_cnx << std::endl;
	//std::cout << "Remaining cnx before : " << _remaining_cnx << std::endl;
	//std::cout <<  "Weights number : " << _w.shape().product() << std::endl;

	_stdp->adapt_parameters(_annealing);

	if(_doPrune || _doReinforcement){
		// dynamic prune
		if(_prune_threshold < _prune_max_threshold){
		_prune_threshold = _prune_threshold + _alpha * (_remaining_cnx / _total_cnx);
		}
		_total_cnx = _remaining_cnx;
		size_t x = 0;
		size_t y = 0;
		size_t z = 0;
		size_t f = 0;
		
		//// apply prune to weights
		for (auto& w : _w)
		{
			if(w < minW){
				minW = w;
			}
			if(w > maxW){
				maxW = w;
			}
			if(w < _prune_threshold){
				if(_doPrune && _cropedVector.at(x,y,z,f) != 1.0f){
					w = 0.0f;
					_croped += 1;
					//std::cout << "cropedVector " << _cropedVector.at(x,y,z) << std::endl;
					//std::cout << w << " : Weight " << _w.at(x,y,z) << std::endl;
					_cropedVector.at(x,y,z,f) = 1.0f;}

			}else{
				if (_doReinforcement){
					//// Apply weight reinforcement
					w = std::min(1.0f,w + (_beta * _prune_threshold));}
			}
			x += 1;
			if(x == _filter_width){
				x = 0;
				y += 1;
				if(y == _filter_height){
					y = 0;
					z += 1;
					if(z == _filter_number){
					z = 0;
					f += 1;
				}
				}
				}
				}

		if(_doReinforcement){
			std::cout << std::endl << "=== Reinforcement applied ===" << std::endl;}
		
		if(_doPrune){
			// update the number of remaining synapses
			_remaining_cnx = (static_cast<int>(_filter_width) * static_cast<int>(_filter_height) * static_cast<int>(_filter_number) * static_cast<int>(_input_depth)) - _croped;

			std::cout << std::endl << "==================================" << std::endl;
			std::cout << std::endl << "Prune threshold : " << _prune_threshold << std::endl;
			std::cout << "Total cnx : " << _total_cnx << std::endl;
			std::cout << "Remaining cnx : " << _remaining_cnx << "(" << ( _croped * 100 ) / (static_cast<int>(_filter_width) * static_cast<int>(_filter_height) * static_cast<int>(_filter_number) * static_cast<int>(_input_depth)) << " %)" << std::endl << std::endl;
			std::cout << std::endl << "==================================" << std::endl;}

	}
}


Tensor<float> Convolution::reconstruct(const Tensor<float>& t) const {
	size_t k = 1;

	size_t output_width = t.shape().dim(0);
	size_t output_height = t.shape().dim(1);
	size_t output_depth = t.shape().dim(2);

	Tensor<float> out(Shape({output_width*_stride_x+_filter_width-1, output_height*_stride_y+_filter_height-1, _input_depth}));
	out.fill(0);

	Tensor<float> norm(Shape({output_width*_stride_x+_filter_width-1, output_height*_stride_y+_filter_height-1, _input_depth}));
	norm.fill(0);

	for(size_t x=0; x<output_width; x++) {
		for(size_t y=0; y<output_height; y++) {

			std::vector<size_t> is;
			for(size_t z=0; z<output_depth; z++) {
				is.push_back(z);
			}

			std::sort(std::begin(is), std::end(is), [&t, x, y](size_t i1, size_t i2) {
				return t.at(x, y, i1) > t.at(x, y, i2);
			});

			for(size_t i=0; i<k; i++) {
				if(t.at(x, y, is[i]) >= 0.0) {
					for(size_t xf=0; xf<_filter_width; xf++) {
						for(size_t yf=0; yf<_filter_height; yf++) {
							for(size_t zf=0; zf<_input_depth; zf++) {
								out.at(x*_stride_x+xf, y*_stride_y+yf, zf) += _w.at(xf, yf, zf, is[i])*t.at(x, y, is[i]);
								norm.at(x*_stride_x+xf, y*_stride_y+yf, zf) += t.at(x, y, is[i]);
							}
						}
					}
				}
			}

		}
	}

	size_t s = out.shape().product();
	for(size_t i=0; i<s; i++) {
		if(norm.at_index(i) != 0)
			out.at_index(i) /= norm.at_index(i);
	}


	out.range_normalize();

	return out;
}

void Convolution::plot_threshold(bool only_in_train) {
	add_plot<plot::Threshold>(only_in_train, _th);
}

void Convolution::plot_evolution(bool only_in_train) {
	add_plot<plot::Evolution>(only_in_train, _w);
}

std::vector<std::vector<std::vector<std::vector<float>>>> Convolution::get_weights(){
	
	std::vector<std::vector<std::vector<std::vector<float>>>> W;

	for(size_t x=0; x<_w.shape().dim(0); x++) {
		std::vector<std::vector<std::vector<float>>> v3d;
		for(size_t y=0; y<_w.shape().dim(1); y++) {
			std::vector<std::vector<float>> v2d;
			for(size_t z=0; z<_w.shape().dim(2); z++) {
				std::vector<float> v1d;
				for(size_t k=0; k<_w.shape().dim(3); k++) {
					v1d.push_back((float)_w.at(x,y,z,k));
				}
				v2d.push_back(v1d);
				}
				v3d.push_back(v2d);
				}
				W.push_back(v3d);
				}
	return W;
}
_priv::DenseImpl::DenseImpl(Convolution& model) : _model(model), _a(), _inh() {

}

void _priv::DenseImpl::resize() {
	_a = Tensor<float>(Shape({_model.width(), _model.height(), _model.depth()}));
	_inh = Tensor<bool>(Shape({_model.width(), _model.height(), _model.depth()}));
}

void _priv::DenseImpl::train(const std::vector<Spike>& input_spike, const Tensor<Time>& input_time, std::vector<Spike>& output_spike) {
	// z = depth : neurons
	// _a : neuron voltage
	// one pass = one input
	size_t depth = _model.depth();
	Tensor<float>& w = _model._w;
	Tensor<float>& th = _model._th;
	std::fill(std::begin(_a), std::end(_a), 0);
	
	// Walk trought all incoming spikes and accumulate them to the neuron membrane 
	for(const Spike& spike : input_spike) {
		// Loop into each neuron of this layer
		for(size_t z=0; z<depth; z++) {
			// accumulate all the inputs of this neuron to the neuron membrane
			_a.at(0, 0, z) += w.at(spike.x, spike.y, spike.z, z);
			// check if the voltage crossed the threshold
			if(_a.at(0, 0, z) >= th.at(z)) {
				_model.layer_Spikes+=1;
				for(size_t z1=0; z1<depth; z1++) {
					th.at(z1) -= _model._lr_th*(spike.time - _model._t_obj);

					if(z1 != z) {
						th.at(z1) -= _model._lr_th/static_cast<float>(depth-1);
					}
					else {
						th.at(z1) += _model._lr_th;
					}
					th.at(z1) = std::max<float>(_model._min_th, th.at(z1));
				}
				// update the weights
				for(size_t x=0; x<_model._filter_width; x++) {
					for(size_t y=0; y<_model._filter_height; y++) {
						for(size_t zi=0; zi<_model._input_depth; zi++) {
							if(_model._cropedVector.at(x, y, zi, z) != 1.0f){
							w.at(x, y, zi, z) = _model._stdp->process(w.at(x, y, zi, z), input_time.at(x, y, zi), spike.time);
							_model.synaptic_updates+=1;
						}
						}
					}
				}
				// inhibition of the rest
				return;
			}
		}
	}
}

void _priv::DenseImpl::test(const std::vector<Spike>& input_spike, const Tensor<Time>&, std::vector<Spike>& output_spike) {
	size_t depth = _model.depth();
	Tensor<float>& w = _model._w;
	Tensor<float>& th = _model._th;


	std::fill(std::begin(_a), std::end(_a), 0);
	std::fill(std::begin(_inh), std::end(_inh), false);

	for(const Spike& spike : input_spike) {

		std::vector<std::tuple<uint16_t, uint16_t, uint16_t, uint16_t>> output_spikes;
		_model.forward(spike.x, spike.y, output_spikes);

		for(const auto& entry : output_spikes) {
			uint16_t x = std::get<0>(entry);
			uint16_t y = std::get<1>(entry);
			uint16_t w_x = std::get<2>(entry);
			uint16_t w_y = std::get<3>(entry);


			for(size_t z=0; z<depth; z++) {

				if(_inh.at(x, y, z)) {
					continue;
				}

				_a.at(x, y, z) += w.at(w_x, w_y, spike.z, z);
				if(_a.at(x, y, z) >= th.at(z)) {
					output_spike.emplace_back(spike.time, x, y, z);
					_inh.at(x, y, z) = true;
				}
			}
		}
	}
}
