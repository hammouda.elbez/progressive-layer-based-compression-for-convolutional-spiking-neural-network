#include "dataset/FMnist.h"

using namespace dataset;

FMnist::FMnist(const std::string& image_filename, const std::string& label_filename, size_t max_read) :
	_image_filename(image_filename), _label_filename(label_filename),
	_image_file(image_filename, std::ios::binary), _label_file(label_filename, std::ios::binary),
	_size(0), _cursor(0), _shape({FMNIST_WIDTH, FMNIST_HEIGHT, FMNIST_DEPTH}), _max_read(max_read) {

	if(!_image_file.is_open()) {
		throw std::runtime_error("Can't open "+image_filename);
	}
	if(!_label_file.is_open()) {
		throw std::runtime_error("Can't open "+label_filename);
	}

	read_header();
}

bool FMnist::has_next() const {
	return _cursor < size();
}


std::pair<std::string, Tensor<InputType>> FMnist::next() {
	assert(!_label_file.eof());
	assert(!_image_file.eof());


	uint8_t label;
	_label_file.read((char*)&label, sizeof(uint8_t));

	std::pair<std::string, Tensor<InputType>> out(std::to_string(static_cast<size_t>(label)), _shape);

	for(size_t y = 0; y < FMNIST_WIDTH; y++) {
		for(size_t x = 0; x < FMNIST_HEIGHT; x++) {
			uint8_t pixel;
			_image_file.read((char*)&pixel, sizeof(uint8_t));
			out.second.at(x, y, 0) = static_cast<InputType>(pixel)/static_cast<InputType>(std::numeric_limits<uint8_t>::max());
		}
	}




	_cursor++;

	return out;
}

void FMnist::reset() {
	_cursor = 0;
	_label_file.seekg(0, std::ios::beg);
	_image_file.seekg(0, std::ios::beg);
	read_header();
}

void FMnist::close() {
	_label_file.close();
	_image_file.close();
}

size_t FMnist::size() const {
	return std::min(_size, _max_read);
}

std::string FMnist::to_string() const {
	return "FMnist("+_image_filename+", "+_label_filename+")["+std::to_string(size())+"]";
}

const Shape& FMnist::shape() const {
	return _shape;
}

void FMnist::read_header() {
	// image file header
	uint32_t image_magic;
	_image_file.read((char*)&image_magic, sizeof(uint32_t));
	image_magic = swap(image_magic);
	_image_file.read((char*)&_size, sizeof(uint32_t));
	_size = swap(_size);
	uint32_t image_width;
	_image_file.read((char*)&image_width, sizeof(uint32_t));
	image_width = swap(image_width);
	uint32_t image_height;
	_image_file.read((char*)&image_height, sizeof(uint32_t));
	image_height = swap(image_height);

	assert(image_width == FMNIST_WIDTH && image_height == FMNIST_HEIGHT);
	assert(image_magic == 0x00000803);

	// label file header
	uint32_t label_magic;
	_label_file.read((char*)&label_magic, sizeof(uint32_t));
	label_magic = swap(label_magic);
	uint32_t label_size;
	_label_file.read((char*)&label_size, sizeof(uint32_t));
	label_size = swap(label_size);

	assert(label_magic == 0x00000801);
	assert(label_size == _size);
}

uint32_t FMnist::swap(uint32_t v) {
	return ((v & 0xFF) << 24) | ((v & 0xFF00) << 8) | ((v & 0xFF0000) >> 8) | ((v & 0xFF000000) >> 24);
}
