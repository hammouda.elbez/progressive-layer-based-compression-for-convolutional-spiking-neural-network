#include "execution/ChunkLayerByLayer.h"

ChunkLayerByLayer::ChunkLayerByLayer(ExperimentType& experiment, const std::string& tmp_dir) :
	_experiment(experiment), _current_input(), _current_output(),
	_rc_width(0), _rc_height(0), _rc_depth(0),_current_layer(0),
	_tmp_dir(tmp_dir), _file_id(0), _readers(), _writers() {

}


void ChunkLayerByLayer::process(size_t refresh_interval) {

	bool is_sorted_c = std::is_sorted(std::begin(_experiment.train_step()), std::end(_experiment.train_step()), [](const auto& e1, const auto& e2) {
		return e1.first < e2.first;
	});

	bool only_unique_c = std::adjacent_find(std::begin(_experiment.train_step()), std::end(_experiment.train_step()), [](const auto& e1, const auto& e2) {
		return e1.first == e2.first;
	}) == std::end(_experiment.train_step());



	if(!is_sorted_c || !only_unique_c) {
		throw std::runtime_error("Layers need to be trained in right order");
	}

	std::pair<size_t, size_t> in_files = _load_data();

	size_t train_id = in_files.first;
	size_t test_id = in_files.second;

	size_t sample_count = 0;

	size_t train_step_index = 0;

	for(size_t i=0; i<_experiment.layer_count(); i++) {

		_prepare(i);

		_current_layer = i;

		_experiment.print() << "Process layer " << _experiment.layer_at(i).name() << std::endl;


		if(train_step_index < _experiment.train_step().size() && _experiment.train_step()[train_step_index].first < i) {
			throw std::runtime_error("Bad train index");
		}
		else if(train_step_index < _experiment.train_step().size() && _experiment.train_step()[train_step_index].first == i) {

			size_t current_epoch = _experiment.train_step()[train_step_index].second;

			for(size_t j=0; j<current_epoch; j++) {
				_experiment.layer_at(i).on_epoch_start();

				size_t k = 0;
				while(_has_next(train_id)) {

					std::pair<std::string, Tensor<float>> data =  _load(train_id);

					_process_sample(data.first, data.second, i);

					_experiment.tick(i, k);

					if(sample_count % refresh_interval == 0) {
						_experiment.print() << "Train layer " << _experiment.layer_at(i).name() << ", Epoch " << (j+1) << "/" << current_epoch << ", Sample " << (k+1) << std::endl;
						_experiment.refresh(i);
					}

					sample_count++;
					k++;
				}

				_experiment.layer_at(i).on_epoch_end();
				_experiment.epoch(i, j);

				_reset_file(train_id);
			}
			train_step_index++;
		}

		in_files = _update_data(i, refresh_interval, train_id, test_id);
		train_id = in_files.first;
		test_id = in_files.second;
	}
	_delete_file(train_id);
	_delete_file(test_id);
}

Tensor<Time> ChunkLayerByLayer::compute_time_at(size_t i) const {
	Tensor<Time> out(Shape({_experiment.layer_at(_current_layer).width(), _experiment.layer_at(_current_layer).height(), _experiment.layer_at(_current_layer).depth()}));
	out.fill(INFINITE_TIME);

	if(i == _current_layer) {
		for(const Spike& spike : _current_input) {
			out.at(spike.x, spike.y, spike.z) = spike.time;
		}
	}
	else if(i == _current_layer+1) {
		for(const Spike& spike : _current_output) {
			out.at(spike.x, spike.y, spike.z) = spike.time;
		}
	}

	return out;
}

void ChunkLayerByLayer::_prepare(size_t layer_target_index) {

	size_t output_width = 1;
	size_t output_height = 1;

	if(layer_target_index >= _experiment.layer_count()) {
		output_width = _experiment.layer_at(layer_target_index).width();
		output_height = _experiment.layer_at(layer_target_index).height();
	}

	auto rc = _experiment.layer_at(layer_target_index).receptive_field_of(std::pair<uint16_t, uint16_t>(output_width, output_height));
	_rc_width = rc.first;
	_rc_height = rc.second;
	_rc_depth = layer_target_index == 0 ? _experiment.input_shape().dim(2) : _experiment.layer_at(layer_target_index-1).depth();

	_experiment.layer_at(layer_target_index).set_size(1, 1);

}

void ChunkLayerByLayer::_process_sample(const std::string& label, const Tensor<Time>& in, size_t layer_index) {

	size_t input_width = layer_index == 0 ? _experiment.input_layer().width(): _experiment.layer_at(layer_index-1).width();
	size_t input_height = layer_index == 0 ? _experiment.input_layer().height() : _experiment.layer_at(layer_index-1).height();
	size_t input_depth = layer_index == 0 ? _experiment.input_layer().depth() : _experiment.layer_at(layer_index-1).depth();


	size_t x = 0;
	size_t y = 0;



	if(_rc_width < input_width) {
		std::uniform_int_distribution<size_t> rand_x(0, input_width-_rc_width);
		x = rand_x(_experiment.random_generator());
	}

	if(_rc_height < input_height) {
		std::uniform_int_distribution<size_t> rand_y(0, input_height-_rc_height);
		y = rand_y(_experiment.random_generator());
	}

	_current_input.clear();

	Tensor<Time> input_time(Shape({_rc_width, _rc_height, input_depth}));

	for(size_t rx=0; rx<_rc_width; rx++) {
		for(size_t ry=0; ry<_rc_height; ry++) {
			for(size_t rz=0; rz<input_depth; rz++) {
				Time t = in.at(x+rx, y+ry, rz);
				input_time.at(rx, ry, rz) = t;
				if(t != INFINITE_TIME) {
					_current_input.emplace_back(t, rx, ry, rz);
				}

			}
		}
	}

	std::sort(std::begin(_current_input), std::end(_current_input), TimeComparator());

	_current_output.clear();
	_experiment.layer_at(layer_index).train(label, _current_input, input_time, _current_output);

}

std::pair<size_t, size_t> ChunkLayerByLayer::_update_data(size_t layer_index, size_t refresh_interval, size_t train_id, size_t test_id) {
	_rc_width = layer_index == 0 ? _experiment.input_layer().width() : _experiment.layer_at(layer_index-1).width();
	_rc_height = layer_index == 0 ? _experiment.input_layer().height() : _experiment.layer_at(layer_index-1).height();
	_rc_depth = layer_index == 0 ? _experiment.input_layer().depth() : _experiment.layer_at(layer_index-1).depth();

	size_t output_width = _experiment.layer_at(layer_index).width();
	size_t output_height = _experiment.layer_at(layer_index).height();
	size_t output_depth = _experiment.layer_at(layer_index).depth();

	std::vector<Output*> outputs;
	for(size_t i=0; i<_experiment.output_count(); i++) {
		if(_experiment.output_at(i).index() == layer_index) {
			outputs.push_back(&_experiment.output_at(i));
		}
	}
	_experiment.layer_at(layer_index).set_size(_experiment.layer_at(layer_index).width(), _experiment.layer_at(layer_index).height());


	size_t next_train_id = _create_file();
	size_t cpt = 0;
	while(_has_next(train_id)) {
		std::vector<Spike> input_spike;
		std::vector<Spike> output_spike;
		std::pair<std::string, Tensor<float>> data = _load(train_id);
		_generate_spike(data.second, input_spike);

		_experiment.layer_at(layer_index).test(data.first, input_spike, data.second, output_spike);

		Tensor<float> output_time(Shape({output_width, output_height, output_depth}));
		output_time.fill(INFINITE_TIME);
		for(const Spike& spike : output_spike) {
			output_time.at(spike.x, spike.y, spike.z) = spike.time;
		}

		_save(next_train_id, data.first, output_time);

		if(cpt % refresh_interval == 0) {
			_experiment.print() << "Convert layer " << _experiment.layer_at(layer_index).name() << " train sample " << (cpt+1) << std::endl;
		}
		cpt++;
	}

	_delete_file(train_id);
	_switch_file(next_train_id);

	size_t next_test_id = _create_file();
	cpt = 0;
	while (_has_next(test_id)) {
		std::vector<Spike> input_spike;
		std::vector<Spike> output_spike;
		std::pair<std::string, Tensor<float>> data = _load(test_id);
		_generate_spike(data.second, input_spike);

		_experiment.layer_at(layer_index).test(data.first, input_spike, data.second, output_spike);

		Tensor<float> output_time(Shape({output_width, output_height, output_depth}));
		output_time.fill(INFINITE_TIME);
		for(const Spike& spike : output_spike) {
			output_time.at(spike.x, spike.y, spike.z) = spike.time;
		}

		_save(next_test_id, data.first, output_time);

		if(cpt % refresh_interval == 0) {
			_experiment.print() << "Convert layer " << _experiment.layer_at(layer_index).name() << " test sample " << (cpt+1) << std::endl;
		}
		cpt++;
	}

	_delete_file(test_id);
	_switch_file(next_test_id);

	// Output
	for(Output* output : outputs) {

		Shape shape({
						_experiment.layer_at(output->index()).width(),
						_experiment.layer_at(output->index()).height(),
						_experiment.layer_at(output->index()).depth()
					});

		size_t out_train_id = _create_file();

		while(_has_next(next_train_id)) {
			std::pair<std::string, Tensor<float>> data = _load(next_train_id);
			_save(out_train_id, data.first, output->converter().process(data.second));
		}

		_switch_file(out_train_id);

		size_t out_test_id = _create_file();

		while(_has_next(next_test_id)) {
			std::pair<std::string, Tensor<float>> data = _load(next_test_id);
			_save(out_test_id, data.first, output->converter().process(data.second));
		}

		_switch_file(out_test_id);

		for(Process* process : output->postprocessing()) {
			_experiment.print() << "Process " << process->class_name() << std::endl;
			out_train_id = _process_train_data(process, out_train_id);
			out_test_id = _process_test_data(process, out_test_id);
		}


		for(Analysis* analysis : output->analysis()) {

			_experiment.log() << output->name() << ", analysis " << analysis->class_name() << ":" << std::endl;

			size_t n = analysis->train_pass_number();

			for(size_t i=0; i<n; i++) {
				analysis->before_train_pass(i);

				while(_has_next(out_train_id)) {
					std::pair<std::string, Tensor<float>> data = _load(out_train_id);
					analysis->process_train_sample(data.first, data.second, i);
				}

				analysis->after_train_pass(i);

				_reset_file(out_train_id);
			}

			if(n == 0) {
				analysis->after_test();
			}
			else {
				analysis->before_test();
				while(_has_next(out_test_id)) {
					std::pair<std::string, Tensor<float>> data = _load(out_test_id);
					analysis->process_test_sample(data.first, data.second);
				}
				analysis->after_test();
				_reset_file(out_test_id);
			}



		}

		_delete_file(out_train_id);
		_delete_file(out_test_id);

	}

	return std::pair<size_t, size_t>(next_train_id, next_test_id);
}

void ChunkLayerByLayer::_generate_spike(const Tensor<Time>& in, std::vector<Spike>& out) {
	for(size_t rx=0; rx<_rc_width; rx++) {
		for(size_t ry=0; ry<_rc_height; ry++) {
			for(size_t rz=0; rz<_rc_depth; rz++) {
				Time t = in.at(rx, ry, rz);
				if(t != INFINITE_TIME) {
					out.emplace_back(t, rx, ry, rz);
				}

			}
		}
	}

	std::sort(std::begin(out), std::end(out), TimeComparator());

}

std::pair<size_t, size_t> ChunkLayerByLayer::_load_data() {
	size_t train_id = _create_file();

	for(Input* input : _experiment.train_data()) {

		size_t count = 0;
		while(input->has_next()) {
			std::pair<std::string, Tensor<float>> data = input->next();
			_save(train_id, data.first, data.second);
			count ++;
		}

		_experiment.log() << "Load " << count << " train samples from " << input->to_string() << std::endl;
		input->close();

	}
	_switch_file(train_id);

	size_t test_id = _create_file();

	for(Input* input : _experiment.test_data()) {

		size_t count = 0;
		while(input->has_next()) {
			std::pair<std::string, Tensor<float>> data = input->next();
			_save(test_id, data.first, data.second);
			count ++;
		}

		_experiment.log() << "Load " << count << " test samples from " << input->to_string() << std::endl;
		input->close();

	}

	_switch_file(test_id);

	for(Process* process : _experiment.preprocessing()) {
		_experiment.print() << "Process " << process->class_name() << std::endl;

		train_id = _process_train_data(process, train_id);
		test_id = _process_test_data(process, test_id);
	}


	size_t out_train_id = _create_file();
	while(_has_next(train_id)) {
		std::pair<std::string, Tensor<float>> data = _load(train_id);
		Tensor<Time> sample(data.second.shape());
		_experiment.input_layer().converter().process(data.second, sample);
		_save(out_train_id, data.first, sample);

	}

	_switch_file(out_train_id);
	_delete_file(train_id);

	size_t out_test_id = _create_file();
	while(_has_next(test_id)) {
		std::pair<std::string, Tensor<float>> data = _load(test_id);
		Tensor<Time> sample(data.second.shape());
		_experiment.input_layer().converter().process(data.second, sample);
		_save(out_test_id, data.first, sample);

	}

	_switch_file(out_test_id);
	_delete_file(test_id);

	return std::pair<size_t, size_t>(out_train_id, out_test_id);
}

size_t ChunkLayerByLayer::_process_train_data(Process* process, size_t in_id) {
	size_t n = process->train_pass_number();

	for(size_t i=0; i<n; i++) {
		size_t out_id = _create_file();

		while(_has_next(in_id)) {
			std::pair<std::string, Tensor<float>> data = _load(in_id);
			process->process_train_sample(data.first, data.second, i);

			if(data.second.shape() != process->shape()) {
				throw std::runtime_error("Unexpected shape (actual: "+data.second.shape().to_string()+", expected: "+process->shape().to_string()+")");
			}

			_save(out_id, data.first, data.second);
		}

		_delete_file(in_id);
		_switch_file(out_id);
		in_id = out_id;


	}

	return in_id;
}

size_t ChunkLayerByLayer::_process_test_data(Process* process, size_t in_id) {
	size_t out_id = _create_file();
	while(_has_next(in_id)) {
		std::pair<std::string, Tensor<float>> data = _load(in_id);
		process->process_test_sample(data.first, data.second);

		if(data.second.shape() != process->shape()) {
			throw std::runtime_error("Unexpected shape (actual: "+data.second.shape().to_string()+", expected: "+process->shape().to_string()+")");
		}
		_save(out_id, data.first, data.second);
	}

	_delete_file(in_id);
	_switch_file(out_id);

	return out_id;
}

size_t ChunkLayerByLayer::_create_file() {
	size_t id = _file_id++;
	_writers.emplace(std::piecewise_construct, std::forward_as_tuple(id), std::forward_as_tuple(_tmp_dir+_experiment.name()+"_"+std::to_string(id)));
	return id;
}

void ChunkLayerByLayer::_switch_file(size_t id) {
	auto in = _writers.find(id);
	if(in == std::end(_writers)) {
		throw std::runtime_error("No tmp write file "+std::to_string(id));
	}
	in->second.close();
	_writers.erase(in);
	_readers.emplace(std::piecewise_construct, std::forward_as_tuple(id), std::forward_as_tuple(_tmp_dir+_experiment.name()+"_"+std::to_string(id)));
}

void ChunkLayerByLayer::_reset_file(size_t id) {
	auto in = _readers.find(id);
	if(in == std::end(_readers)) {
		throw std::runtime_error("No tmp read file "+std::to_string(id));
	}
	in->second.reset();
}

void ChunkLayerByLayer::_delete_file(size_t id) {
	auto in = _readers.find(id);
	if(in == std::end(_readers)) {
		throw std::runtime_error("No tmp read file "+std::to_string(id));
	}
	in->second.close();
	std::string filename = _tmp_dir+_experiment.name()+"_"+std::to_string(id);
	if(std::remove(filename.c_str()) != 0) {
		throw std::runtime_error("Unable to delete "+filename);
	}
	_readers.erase(in);
}


bool ChunkLayerByLayer::_has_next(size_t id) {
	auto in = _readers.find(id);
	if(in == std::end(_readers)) {
		throw std::runtime_error("No tmp read file "+std::to_string(id));
	}
	return in->second.has_next();
}

std::pair<std::string, Tensor<float>> ChunkLayerByLayer::_load(size_t id) {
	auto in = _readers.find(id);
	if(in == std::end(_readers)) {
		throw std::runtime_error("No tmp read file "+std::to_string(id));
	}
	return in->second.next();
}


void ChunkLayerByLayer::_save(size_t id, const std::string& label, const Tensor<float>& tensor) {
	auto in = _writers.find(id);
	if(in == std::end(_writers)) {
		throw std::runtime_error("No tmp write file "+std::to_string(id));
	}
	in->second.write(label, tensor);
}
