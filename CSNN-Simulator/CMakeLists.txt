project(FalezCSNNSimulator)
cmake_minimum_required(VERSION 3.1)

set(CMAKE_CXX_STANDARD 14)
set(LIBRARY_NAME "snn_v2" CACHE STRING "Name of library")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")
set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
find_package(Qt4 4.4.3 REQUIRED QtCore QtGui)
find_package(Threads REQUIRED)

file(GLOB_RECURSE SOURCES src/* include/*)

include_directories(include/)

find_package(mongocxx REQUIRED)
find_package(bsoncxx REQUIRED)
include_directories(${LIBMONGOCXX_INCLUDE_DIR})
include_directories(${LIBBSONCXX_INCLUDE_DIR})
#include_directories("/usr/local/include/libmongoc-1.0/mongoc/")

set(APPS_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -ansi -pedantic -Wshadow -Weffc++ -ftemplate-backtrace-limit=0  -msse -msse2 -msse3 -march=native -mfpmath=sse")
# add_definitions(-DSMID_AVX256)
add_definitions(-DDISPLAY_PLOT)

add_subdirectory(dep/qcustomplot)
add_subdirectory(dep/libsvm)
#
#   Build library
#

add_library(${LIBRARY_NAME} SHARED ${SOURCES})
set_target_properties(${LIBRARY_NAME} PROPERTIES COMPILE_FLAGS ${APPS_FLAGS})
target_link_libraries(${LIBRARY_NAME} qcustomplot libsvm Qt4::QtCore Qt4::QtGui Threads::Threads mongo::bsoncxx_shared mongo::mongocxx_shared)

#
#   Build executables
#
file(GLOB_RECURSE apps apps/*)
foreach(app ${apps})
    get_filename_component(app_name ${app} NAME_WE)
    add_executable(${app_name} ${app})
    set_target_properties(${app_name} PROPERTIES COMPILE_FLAGS ${APPS_FLAGS})
    target_link_libraries(${app_name} ${LIBRARY_NAME} Qt4::QtCore Qt4::QtGui Threads::Threads)
endforeach()
set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")