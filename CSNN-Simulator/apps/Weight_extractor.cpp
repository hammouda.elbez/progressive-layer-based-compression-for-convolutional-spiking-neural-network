#include "Experiment.h"
#include "Tensor.h"
#include "layer/Convolution.h"
#include "execution/OptimizedLayerByLayer.h"

int main(int argc, char** argv) {

	Experiment<OptimizedLayerByLayer> experiment(argc, argv, "weights_extractor");
	// Sim load
	experiment.load(argv[1]);
	std::string layer_name = argv[2];
	experiment.initialize(Shape({28, 28, 1}));

	std::vector<std::vector<std::vector<std::vector<float>>>> loaded_weights = dynamic_cast<layer::Convolution&>(experiment.layer(layer_name)).get_weights();

	// Save to another binary
	std::ofstream file("weights_"+experiment.layer(layer_name).name(), std::ios::out | std::ios::trunc | std::ios::binary);

	file.imbue(std::locale("C.UTF-8"));
	
	for(size_t x = 0; x < loaded_weights.size(); x++ ){

	   for(size_t y = 0; y < loaded_weights[x].size(); y++ ){

		   for(size_t z = 0; z < loaded_weights[x][y].size(); z++ ){

			const char* buffer = (char*)(&loaded_weights[x][y][z][0]);
			file.write(buffer, loaded_weights[x][y][z].size() * sizeof(float));
 
			}
	   }
	}
	
	file.close();
}