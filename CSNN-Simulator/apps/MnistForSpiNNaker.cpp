#include "Experiment.h"
#include "dataset/Mnist.h"
#include "stdp/Simplified.h"
#include "layer/Convolution.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"

int main(int argc, char** argv) {

	std::string name = "mnist_spinn";
	float do_prune = atof(argv[1]);
	float do_reinforcement = atof(argv[2]);

	if(do_prune == 1){
		name = name + "_pruned";
	}
	if(do_reinforcement == 1){
		name = name + "_reinforced";
	}
	
	Experiment<OptimizedLayerByLayer> experiment(argc, argv, name);

	experiment.template input<LatencyCoding>();
	
	const char* input_path_ptr = "../Datasets/MNIST/";

	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	std::string input_path(input_path_ptr);

	experiment.template add_train<dataset::Mnist>(input_path+"train-images.idx3-ubyte", input_path+"train-labels.idx1-ubyte");
	experiment.template add_test<dataset::Mnist>(input_path+"t10k-images.idx3-ubyte", input_path+"t10k-labels.idx1-ubyte");

	float th_lr = 1.0f;
	float t_obj = 0.75f;
	float alpha = 0.05f;
	float alpha_p= 0.01f;
	float alpha_n= 0.005f;
	float beta_p= 1.5f;
	float beta_n= 2.5f;
	float prune_max_threshold = 0.7f;

	auto& fc1 = experiment.template push_layer<layer::Convolution>("fc1", 28, 28, 50);
	fc1.template parameter<float>("annealing").set(0.95f);
	fc1.template parameter<float>("min_th").set(1.0f);
	fc1.template parameter<float>("t_obj").set(t_obj);
	fc1.template parameter<float>("lr_th").set(th_lr);
	fc1.template parameter<float>("doPrune").set(do_prune);
	fc1.template parameter<float>("doReinforcement").set(do_reinforcement);
	fc1.template parameter<float>("prune_max_threshold").set(prune_max_threshold);
	fc1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	fc1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(8.0, 0.1);
	fc1.template parameter<STDP>("stdp").template set<stdp::Simplified>(alpha_p,alpha_n,beta_p,beta_n);

	alpha = alpha*2;

	auto& fc2 = experiment.template push_layer<layer::Convolution>("fc2", 1, 1, 128);
	fc2.template parameter<float>("annealing").set(0.95f);
	fc2.template parameter<float>("min_th").set(1.0f);
	fc2.template parameter<float>("t_obj").set(t_obj);
	fc2.template parameter<float>("lr_th").set(th_lr);
	fc2.template parameter<float>("doPrune").set(do_prune);
	fc2.template parameter<float>("doReinforcement").set(do_reinforcement);
	fc2.template parameter<float>("prune_max_threshold").set(prune_max_threshold);
	fc2.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	fc2.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
	fc2.template parameter<STDP>("stdp").template set<stdp::Simplified>(alpha_p,alpha_n,beta_p,beta_n);

	experiment.add_train_step(fc1, 10);
	experiment.add_train_step(fc2, 10);

	experiment.run(10000);

	return experiment.wait();

}