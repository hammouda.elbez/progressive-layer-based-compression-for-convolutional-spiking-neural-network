#include "Experiment.h"
#include "dataset/Mnist.h"
#include "stdp/Biological.h"
#include "layer/Convolution.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"
#include "analysis/Svm.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "layer/Pooling.h"
#include "process/OnOffFilter.h"
#include "process/Scaling.h"
#include "process/Pooling.h"

int main(int argc, char** argv) {

	std::string name = "mnist";
	float do_prune = atof(argv[1]);
	float do_reinforcement = atof(argv[2]);
	// change this for the layerwise pruning
	float layerwise_prune = false;

	if(do_prune == 1){
		name = name + "_pruned";
	}
	if(do_reinforcement == 1){
		name = name + "_reinforced";
	}
	
	Experiment<OptimizedLayerByLayer> experiment(argc, argv, name);

	experiment.template add_preprocessing<process::DefaultOnOffFilter>(7, 1.0, 4.0);
	experiment.template add_preprocessing<process::FeatureScaling>();
	experiment.template input<LatencyCoding>();
	
	const char* input_path_ptr = "../Datasets/MNIST/";

	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	std::string input_path(input_path_ptr);

	experiment.template add_train<dataset::Mnist>(input_path+"train-images.idx3-ubyte", input_path+"train-labels.idx1-ubyte");
	experiment.template add_test<dataset::Mnist>(input_path+"t10k-images.idx3-ubyte", input_path+"t10k-labels.idx1-ubyte");

	float th_lr = 1.0f;
	float t_obj = 0.75f;
	float w_lr = 0.1f;
	float alpha = 0.05f;

	auto& conv1 = experiment.template push_layer<layer::Convolution>("conv1", 5, 5, 32);
	conv1.template parameter<float>("annealing").set(0.95f);
	conv1.template parameter<float>("min_th").set(1.0f);
	conv1.template parameter<float>("t_obj").set(t_obj);
	conv1.template parameter<float>("lr_th").set(th_lr);
	conv1.template parameter<float>("alpha").set(alpha);
	conv1.template parameter<float>("doPrune").set(do_prune);
	conv1.template parameter<float>("doReinforcement").set(do_reinforcement);
	conv1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	conv1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(8.0, 0.1);
	conv1.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

	experiment.template push_layer<layer::Pooling>("pool1", 2, 2, 2, 2);

	if(layerwise_prune){
		alpha = alpha * 2 ;
	}
	auto& conv2 = experiment.template push_layer<layer::Convolution>("conv2", 5, 5, 128);
	conv2.template parameter<float>("annealing").set(0.95f);
	conv2.template parameter<float>("min_th").set(1.0f);
	conv2.template parameter<float>("t_obj").set(t_obj);
	conv2.template parameter<float>("lr_th").set(th_lr);
	conv2.template parameter<float>("alpha").set(alpha);
	conv2.template parameter<float>("doPrune").set(do_prune);
	conv2.template parameter<float>("doReinforcement").set(do_reinforcement);
	conv2.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	conv2.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
	conv2.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

	experiment.template push_layer<layer::Pooling>("pool2", 2, 2, 2, 2);

	if(layerwise_prune){
		alpha = alpha * 2 ;
	}
	auto& fc1 = experiment.template push_layer<layer::Convolution>("fc1", 4, 4, 1024);
	fc1.template parameter<float>("annealing").set(0.95f);
	fc1.template parameter<float>("min_th").set(1.0f);
	fc1.template parameter<float>("t_obj").set(t_obj);
	fc1.template parameter<float>("lr_th").set(th_lr);
	fc1.template parameter<float>("alpha").set(alpha);
	fc1.template parameter<float>("doPrune").set(do_prune);
	fc1.template parameter<float>("doReinforcement").set(do_reinforcement);
	fc1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	fc1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
	fc1.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

	experiment.add_train_step(conv1, 5);
	experiment.add_train_step(conv2, 5);
	experiment.add_train_step(fc1, 5);

	auto& conv1_out = experiment.template output<TimeObjectiveOutput>(conv1, t_obj);
	conv1_out.template add_postprocessing<process::SumPooling>(2, 2);
	conv1_out.template add_postprocessing<process::FeatureScaling>();

	auto& conv2_out = experiment.template output<TimeObjectiveOutput>(conv2, t_obj);
	conv2_out.template add_postprocessing<process::SumPooling>(2, 2);
	conv2_out.template add_postprocessing<process::FeatureScaling>();

	auto& fc1_out = experiment.template output<TimeObjectiveOutput>(fc1, t_obj);
	fc1_out.template add_postprocessing<process::FeatureScaling>();
	fc1_out.template add_analysis<analysis::Svm>();

	experiment.run(10000);

	return experiment.wait();

}