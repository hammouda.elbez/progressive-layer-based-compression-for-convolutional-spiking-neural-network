#ifndef _STDP_SIMPLIFIED_H
#define _STDP_SIMPLIFIED_H

#include "Stdp.h"

namespace stdp {

	class Simplified : public STDP {

	public:
		Simplified();
		Simplified(float alpha_p, float alpha_n, float beta_p, float beta_n);

		virtual float process(float w, const Time pre, Time post);
		virtual void adapt_parameters(float factor);
	private:
		float _alpha_p;
		float _alpha_n;
		float _beta_p;
		float _beta_n;
	};

}
#endif
