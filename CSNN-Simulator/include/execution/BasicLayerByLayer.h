#ifndef _EXECUTION_BASIC_LAYER_BY_LAYER_H
#define _EXECUTION_BASIC_LAYER_BY_LAYER_H

#include "Experiment.h"
#include "SpikeConverter.h"

class BasicLayerByLayer {

public:
	typedef Experiment<BasicLayerByLayer> ExperimentType;

	BasicLayerByLayer(ExperimentType& experiment);

	void process(size_t refresh_interval);

	Tensor<Time> compute_time_at(size_t i) const;
private:
	void _prepare(size_t layer_target_index);
	void _process_sample(const std::string& label, const Tensor<float>& values, bool train);
	void _convert_input(const Tensor<float>& in, size_t offset_x, size_t offset_y, std::vector<Spike>& input_spikes);

	void _load_data();
	void _process_train_data(Process* process, std::vector<std::pair<std::string, Tensor<float>>>& data);
	void _process_test_data(Process* process, std::vector<std::pair<std::string, Tensor<float>>>& data);

	ExperimentType& _experiment;

	std::vector<std::pair<std::string, Tensor<float>>> _train_set;
	std::vector<std::pair<std::string, Tensor<float>>> _test_set;

	std::vector<Tensor<Time>> _current_time;

};

#endif
