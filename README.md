# Progressive Layer-based Compression for Convolutional Spiking Neural Network 
Here you find the code for the paper `Progressive Layer-based Compression for Convolutional Spiking Neural Network`. <!--([link](https://hal.archives-ouvertes.fr/hal-03826823))-->

## CSNN

### Requirement for the simulator

* C++ compiler (version >= 14)
* Cmake (version >= 3.1)
* Qt4 (version >= 4.4.3)

### Building the binaries

Run the following commands inside the CSNN folder:

    mkdir build
    cd build
    cmake ../ -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS='-std=c++11'
    make
Remember to build again if you change the source code.

### How to use CSNN
Once the `make` command is done, you should see binary files representing each simulation.

Run a simulation:

    ./[sim_name] x y
    
    args:
        x = enable PP (pruning) [0 or 1]
        y = enable DSWR (reinforcement) [0 or 1]

For example: to run MNIST simulation without compression and reinforcement:

    ./Mnist 0 0

In the `apps` folder, you will find the source code for each simulation, where you can change the architecture and the network parameters or activate the [layerwise compression](https://gitlab.univ-lille.fr/hammouda.elbez/progressive-layer-based-compression-for-convolutional-spiking-neural-network/-/blob/main/CSNN-Simulator/apps/Mnist.cpp#L21).

### How to use Norse
First, install the required libraries:

    python install -r requirements.txt

To run a simulation:

    python [name_dataset].py

Each file runs each configuration ten times. You can change the parameters at the beginning of each file.
## Going from CSNN to SpiNNaker
First, run the simulation of the two dense layers using CSNN:
    
    ./MnistForSpiNNaker 0 0

then

    ./MnistForSpiNNaker 1 1

To transfer the learned weights from CSNN to SpiNNaker, we use the following command:

    ./Weight_extractor [binary file generated from a simulation] [name_layer]

For example:

    ./Weight_extractor mnist_params conv1
> `weights_conv1` is generated

This will generate another binary file (named weights_[name_layer]) which contains only the weights of the selected layer.

## How to use SpiNNaker scripts
To setup the SpiNNaker board, please check the following link:

http://spinnakermanchester.github.io/

### Weights adaptation for PyNN and SpiNNaker

in SpiNNaker folder:

Run the `ConvertTheWeights.ipynb` notebook to adapt the extracted weights from CSNN to a text format readable by PyNN.

### Using the extracted weights with SpiNNaker

Run the `SpiNNakerRun.ipynb` notebook to deploy the weights on the board and run the simulation.

## Folder structure

```
CSNN            # The C++ Simulator of Convolutional Spiking Neural Network
SpiNNaker       # The scripts in python which are used for running on the SpiNNaker board
```

<!--# Citation

If you found our work useful, please don't forget to cite:

> Hammouda Elbez, Mazdak Fatahi. Progressive Layer-based Compression for Convolutional Spiking Neural Network. 2022. ⟨hal-03826823⟩
-->