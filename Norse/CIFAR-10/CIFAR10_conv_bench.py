import sys
sys.path.append('../')

import torch
import numpy as np
from compression import ProgressiveCompression
from norse.torch import LIFCell
from norse.torch.module.leaky_integrator import LILinearCell
from norse.torch import LIFParameters
from norse.torch.module import encode
from datetime import datetime
import torchvision
import os
import pickle
import random
import itertools

# Reproducibility
torch.manual_seed(0)
random.seed(0)
np.random.seed(0)

MAXTH = [0.3,0.4,0.5,0.6,0.7]
ALPHA = [0.005]
REINFORCEMENT = [True]
COMPRESSION = [False, True]
LAYERWISE = [False, True]

for maxTh, Alpha, reinforcement, compression, layerwise in np.array(list(itertools.product(MAXTH, ALPHA, REINFORCEMENT, COMPRESSION, LAYERWISE))):
    try: 
        os.mkdir("CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement))
    except OSError as error: 
        print(error) 

    for i in range(10):
        before = datetime.now()

        file = open("CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement)+"/CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement)+"_"+str(before), 'w+')
        
        transform = torchvision.transforms.Compose(
        [
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize((0.1307,), (0.3081,)),
        ]
        )

        train_data = torchvision.datasets.CIFAR10(
            root=".",
            train=True,
            download=True,
            transform=transform,
        )

        # reduce this number if you run out of GPU memory
        BATCH_SIZE = 32

        train_loader = torch.utils.data.DataLoader(
            train_data, batch_size=BATCH_SIZE, shuffle=True #, sampler=SubsetRandomSampler(list(range(len(train_data)))[0:1000]) #
        )

        test_loader = torch.utils.data.DataLoader(
            torchvision.datasets.CIFAR10(
                root=".",
                train=False,
                transform=transform
            ),
            batch_size=BATCH_SIZE
        )

        class Model(torch.nn.Module):
            def __init__(self, encoder, snn, decoder):
                super(Model, self).__init__()
                self.encoder = encoder
                self.snn = snn
                self.decoder = decoder

            def forward(self, x):
                x = self.encoder(x)
                x = self.snn(x)
                log_p_y = self.decoder(x)
                return log_p_y

        class ConvNet(torch.nn.Module):
            def __init__(self, num_channels=3, feature_size=32, method="super", alpha=100):
                super(ConvNet, self).__init__()

                self.conv1_out_channels = 128
                self.conv2_out_channels = 256
                self.fc1_out_channels = 1024
                self.out_channels = 10
                self.conv1 = torch.nn.Conv2d(num_channels, self.conv1_out_channels, 5, 1, bias=False)
                self.conv2 = torch.nn.Conv2d(self.conv1_out_channels, self.conv2_out_channels, 5, 1, bias=False)
                self.fc1 = torch.nn.Linear(256*5*5, self.fc1_out_channels, bias=False)
                self.fc2 = torch.nn.Linear(self.fc1_out_channels, 1000, bias=False)
                self.lif0 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.3))
                self.lif1 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.3))
                self.lif2 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.3))
                self.lif3 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.3))
                self.out = LILinearCell(1000, self.out_channels,p=LIFParameters(method=method, alpha=alpha, v_th=0.3))
                
            def forward(self, x):
                seq_length = x.shape[0]
                batch_size = x.shape[1]

                # specify the initial states
                s0 = s1 = s2 = s3 = s4 = so = None

                voltages = torch.zeros(
                    seq_length, batch_size, self.out_channels, device=x.device, dtype=x.dtype
                )

                for ts in range(seq_length):
                    z = torch.nn.functional.relu(self.conv1(x[ts, :]))

                    z, s0 = self.lif0(z, s0)
                    z = torch.nn.functional.max_pool2d(z, 2, 2)
                    z = 3 * torch.nn.functional.relu(self.conv2(z))

                    z, s1 = self.lif1(z, s1)
                    z = torch.nn.functional.max_pool2d(z, 2, 2)

                    z = z.view(-1, 256*5*5)
                    z = self.fc1(z)
                    z, s2 = self.lif2(z, s2)
                    z = self.fc2(z)
                    z, s3 = self.lif3(z, s3)
                    v, so = self.out(z, so)

                    voltages[ts, :, :] = v

                return voltages

        def train(model, device, train_loader, optimizer, epoch, max_epochs):
            model.train()
            losses = []

            for (data, target) in train_loader:
                data, target = data.to(device), target.to(device)
                optimizer.zero_grad()
                output = model(data)
                loss = torch.nn.functional.nll_loss(output, target)
                loss.backward()
                optimizer.step()
                losses.append(loss.item())

            mean_loss = np.mean(losses)
            return losses, mean_loss

        def test(model, device, test_loader, epoch):
            model.eval()
            test_loss = 0
            correct = 0
            with torch.no_grad():
                for data, target in test_loader:
                    data, target = data.to(device), target.to(device)
                    output = model(data)
                    test_loss += torch.nn.functional.nll_loss(
                        output, target, reduction="sum"
                    ).item()  # sum up batch loss
                    pred = output.argmax(
                        dim=1, keepdim=True
                    )  # get the index of the max log-probability
                    correct += pred.eq(target.view_as(pred)).sum().item()

            test_loss /= len(test_loader.dataset)

            accuracy = 100.0 * correct / len(test_loader.dataset)

            return test_loss, accuracy

        def decode(x):
            x, _ = torch.max(x, 0)
            log_p_y = torch.nn.functional.log_softmax(x, dim=1)
            return log_p_y

        torch.autograd.set_detect_anomaly(True)

        T = 35
        LR = 3e-5
        EPOCHS = 100  # Increase this for improved accuracy

        if torch.cuda.is_available():
            DEVICE = torch.device("cuda")
        else:
            DEVICE = torch.device("cpu")

        model = Model(
            encoder=encode.SpikeLatencyLIFEncoder(T,p=LIFParameters(v_th=0.3)), snn=ConvNet(alpha=80), decoder=decode).to(DEVICE)

        optimizer = torch.optim.Adam(model.parameters(), lr=LR)

        # compression
        if (compression):
            progressive_compression = ProgressiveCompression(NorseModel=model, maxThreshold=maxTh, alphaP=Alpha, alphaN=-Alpha,  to_file=True, apply_reinforcement=reinforcement, file= file, layerwise=layerwise)

        training_losses = []
        mean_losses = []
        test_losses = []
        accuracies = []

        for epoch in range(EPOCHS):
            print(f"Epoch {epoch}")
            training_loss, mean_loss = train(
                model, DEVICE, train_loader, optimizer, epoch, max_epochs=EPOCHS
            )
            test_loss, accuracy = test(model, DEVICE, test_loader, epoch)
            training_losses += training_loss
            mean_losses.append(mean_loss)
            test_losses.append(test_loss)
            accuracies.append(accuracy)
            if (compression):
                progressive_compression.apply()

        print(f"final accuracy: {accuracies[-1]}")
        file.write("final accuracy:"+str(accuracies[-1])+"\n")
        file.write("time:"+str(datetime.now() - before)+"\n")
        

        torch.save(model,"CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement)+"/CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement)+"_"+str(before)+".norse")
        if (compression):
            torch.save([mean_losses,test_losses,accuracies,progressive_compression.weights,progressive_compression.compressions,progressive_compression.thresholds_p,progressive_compression.thresholds_n], "CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement)+"/CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement)+"_"+str(before)+".pkl")
        else:
            torch.save([mean_losses,test_losses,accuracies], "CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement)+"/CIFAR10_CONV_maxTh:"+str(maxTh)+"_Alpha:"+str(Alpha)+"_"+"reinforcement:"+str(reinforcement)+"_"+str(before)+".pkl")
