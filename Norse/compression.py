import numpy as np
import torch

class ProgressiveCompression:
    def __init__(self, NorseModel,maxThreshold=0.3, alphaP=0.005, alphaN=-0.005, betaP=0.01, betaN=-0.01, prune_recurrent=True, apply_reinforcement=False, to_file=False, file=None, layerwise=False):
        self.alpha_p = []
        self.alpha_n = []
        self.max_threshold = maxThreshold
        self.model = NorseModel
        self.prune_recurrent = prune_recurrent
        self.layerwise = layerwise
        self.apply_reinforcement = apply_reinforcement
        self.to_file = to_file
        self.file = file
        self.max_threshold_p = []
        self.max_threshold_n = []
        self.max_w_p = []
        self.max_w_n = []
        self.prune_matrix = []
        self.thresholds_p = []
        self.thresholds_n = []
        self.compressions = []
        self.weights = []
        self.betaP = betaP
        self.betaN = betaN

        i = 0
        for name, param in self.model.named_parameters():
            print(name,param.data.min(),param.data.max())
            self.max_w_p.append(param.data.max())
            self.max_w_n.append(param.data.min())
            
            if (self.layerwise):
                self.alpha_p.append(alphaP + (0.005*i))
                self.alpha_n.append(alphaN + (-0.005*i))
            else:
                self.alpha_p.append(alphaP)
                self.alpha_n.append(alphaN)
                
            self.max_threshold_p.append(param.data.max() * self.max_threshold)
            self.max_threshold_n.append(param.data.min() * self.max_threshold)
            print("max_threshold_n: "+str(param.data.min() * self.max_threshold)+" max_threshold_p: "+str(param.data.max() * self.max_threshold)+"\n")
            if(self.to_file):
                self.file.write(str(name)+"  "+str(param.data.min())+"  "+str(param.data.max())+"\n")
                self.file.write("max_threshold_n: "+str(param.data.min() * self.max_threshold)+" max_threshold_p: "+str(param.data.max() * self.max_threshold)+"\n")
            i = i + 1

        for param in self.model.parameters():
            self.prune_matrix.append(torch.zeros_like(param))
            self.thresholds_p.append([None])
            self.thresholds_n.append([None])
            self.compressions.append([0])

    def applyprune(self,name, alpha_p, alpha_n, max_thresholds_p,max_thresholds_n, weights, prune_matrix, threshold_p, threshold_n):
        print(name, " before prune: Min: ",weights.min()," Max: ",weights.max())
        if ((not self.prune_recurrent) and ("recurrent" in name)):
            return weights, prune_matrix, threshold_p, threshold_n
        else:
            if threshold_p == None:
                threshold_p = alpha_p
            else:
                if threshold_p < max_thresholds_p:
                    threshold_p += alpha_p * (np.count_nonzero(prune_matrix.cpu() == 0) / (np.count_nonzero(prune_matrix.cpu() == 0) + np.count_nonzero(prune_matrix.cpu() == 1)))
                    threshold_p = round(threshold_p, 6)

            if threshold_n == None:
                threshold_n = alpha_n
            else:
                if threshold_n > max_thresholds_n:
                    threshold_n += alpha_n * (np.count_nonzero(prune_matrix.cpu() == 0) / (np.count_nonzero(prune_matrix.cpu() == 0) + np.count_nonzero(prune_matrix.cpu() == 1)))
                    threshold_n = round(threshold_n, 6)

            return weights.masked_fill(((weights < threshold_p) & (weights > 0)) | ((weights > threshold_n) & (weights < 0)), 0), prune_matrix.masked_fill(((weights < threshold_p) & (weights > 0)) | ((weights > threshold_n) & (weights < 0)), 1), threshold_p, threshold_n

    def reinforcement(self, name, weights, beta_p, beta_n, thres_p, thres_n, max_w_p, max_w_n):
        if ((not self.prune_recurrent) and ("recurrent" in name)):
            return weights
        weights = torch.where(weights > 0,weights + beta_p * thres_p,weights)
        weights[weights > max_w_p] = max_w_p
        weights = torch.where(weights < 0,weights - beta_n * thres_n,weights)
        weights[weights < max_w_n] = max_w_n
        return weights

    def apply(self,):
        i = 0
        for name, param in self.model.named_parameters():
            param.data, self.prune_matrix[i], thres_p , thres_n = self.applyprune(name, self.alpha_p[i],self.alpha_n[i],self.max_threshold_p[i],self.max_threshold_n[i],param.data,self.prune_matrix[i],self.thresholds_p[i][-1],self.thresholds_n[i][-1])
            #try:
            print(name,"zeros:",int((self.prune_matrix[i]).sum()),"/",float(torch.prod(torch.tensor(param.data.shape))),"("+str(round(float((self.prune_matrix[i]).sum()*100/(torch.prod(torch.tensor(param.data.shape)))),3))+"%)","threshold_n:",thres_n,"threshold_p:",thres_p)
            if(self.to_file):
                self.file.write(str(name)+"  zeros:  "+str(float((self.prune_matrix[i]).sum()))+"  /  "+str(float(torch.prod(torch.tensor(param.data.shape))))+"  ("+str(round(float((self.prune_matrix[i]).sum()*100/(torch.prod(torch.tensor(param.data.shape)))),3))+"%)  threshold_n:  "+str(thres_n)+"  threshold_p:  "+str(thres_p)+"\n")
            self.compressions[i].append(round(float((self.prune_matrix[i]).sum()*100/(torch.prod(torch.tensor(param.data.shape)))),3))
            self.thresholds_p[i].append(thres_p)
            self.thresholds_n[i].append(thres_n)
            if(self.apply_reinforcement):
                param.data = self.reinforcement(name, param.data, self.betaP, self.betaN, thres_p, thres_n, self.max_w_p[i], self.max_w_n[i])
            self.weights.append(param.data)
            #except Exception:
            #    pass

            i+=1
